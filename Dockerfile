FROM node:20 AS builder
WORKDIR /builder
COPY package*.json /builder/
RUN npm install
COPY lib ./lib
RUN npm run build

FROM node:20
WORKDIR /app
COPY package*.json /app/
COPY --from=builder /builder/dist /app/dist/
RUN npm install --package-lock-only
RUN npm ci --omit=dev
COPY config.yml /app/
EXPOSE 8442
CMD ["node", "dist/index.js"]

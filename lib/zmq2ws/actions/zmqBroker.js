import zmq from "zeromq";
import Rx from "rxjs";
import * as zmqBroker from "../actions/zmqBroker";
import * as appInfo from "../actions/appInfo";

import {
  ON_ZMQ_BROKER_ADD,
  ON_ZMQ_BROKER_REMOVE,
  ON_ZMQ_BROKER_ZMQSUB_ADD,
  ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_GROUP,
  ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_FULLNAME
} from "../actionTypes/zmqBroker";

export const onZeroMQAdd = (data, wss) => (dispatch, getState) => {
  const state = getState();

  // TODO If we already have that name
  let zmq_sock = zmq.socket("sub");
  zmq_sock.connect(data.url);
  zmq_sock.subscribe("");
  dispatch({
    type: ON_ZMQ_BROKER_ADD,
    payload: data,
    zmq: zmq_sock
  });

  zmq_sock.on("message", (sub, name, d) => {
    //    console.log("zmq : '" + sub.toString() + "' '" + name.toString() + "'");

    const state = getState();

    let sub_str = sub.toString();
    let src = sub_str.substring(0, sub_str.indexOf(":"));
    if (src === "") src = "obmon";


    let n = src + "|" + data.name + "|" + name.toString();
    // FIXME IF ZMQ service is removed
    if (state.zmqBroker.zmqs$[n] === undefined) {
      // console.log(data)
      dispatch(
        zmqBroker.onZeroMQSubAdd({ name: data.name, sub: name.toString(), src: src.toString(), cache: data.cache || 1 })
      );
      dispatch(
        appInfo.onZeroMQServiceAdd(
          { name: data.name, sub: name.toString(), src: src.toString() },
          wss
        )
      );
    }

    if (state.zmqBroker.zmqs$[n] !== undefined) {
      let r$ = state.zmqBroker.zmqs$[n].zmq$;
      if (r$ !== undefined) {
        let out = {
          name: data.name,
          sub: name.toString(),
          data: JSON.parse(d.toString())
        };
        r$.next(out);
      }
    }
  });
};
export const onZeroMQRemove = (data, wss) => (dispatch, getState) => {
  const state = getState();

  if (state.zmqBroker.zmqs === undefined) {
    return;
  }
  let z = state.zmqBroker.zmqs[data.name];
  if (z === undefined) {
    return;
  }

  z.zmq.disconnect(z.url);
  delete z.zmq;

  dispatch(appInfo.onZeroMQServiceRemove({ name: data.name, sub: "" }, wss));
  dispatch({
    type: ON_ZMQ_BROKER_REMOVE,
    payload: data
  });
};

export const onZeroMQSubAdd = data => (dispatch, getState) => {
  const state = getState();
  dispatch({
    type: ON_ZMQ_BROKER_ZMQSUB_ADD,
    payload: data,
    zmq$: new Rx.ReplaySubject(data.cache || 1)
  });
};

export const onZeroMQSubRemoveByGroup = data => (dispatch, getState) => {
  const state = getState();
  dispatch({
    type: ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_GROUP,
    payload: data
  });
};

export const onZeroMQSubRemoveByFullName = data => (dispatch, getState) => {
  const state = getState();
  dispatch({
    type: ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_FULLNAME,
    payload: data
  });
};

export const onZeroMQSubCleanup = (timeout, wss) => (dispatch, getState) => {

  var d = new Date();
  d.setSeconds(d.getSeconds() - timeout);
  const state = getState();
  // console.log("Checking cleanup ...");

  for (var key in state.zmqBroker.zmqs$) {
    var obj = state.zmqBroker.zmqs$[key];
    // console.log(key);
    // console.log(obj.zmq$._events.length);
    if (obj.zmq$._events.length > 0) {
      // console.log(d.getTime());
      // console.log(obj.zmq$._events[0].time);
      let diff = d.getTime() - obj.zmq$._events[0].time;
      // console.log("key : " + key + " diff : " + diff);
      if (diff > 0) {
        var res = key.split("|");
        console.log("Removing inactive service (more then " + timeout + " sec) : name=" + res[1] + " sub=" + res[2] + " src=" + res[0] + "...");
        dispatch(zmqBroker.onZeroMQSubRemoveByFullName({ name: key }));
        dispatch(appInfo.onZeroMQServiceRemoveSingle({ name: res[1], sub: res[2], src: res[0] }, wss));
        // sending empty data to signal frontend that service is not active anymore
        wss.broadcast(JSON.stringify({ type: "zmq", data: [], name: res[1], sub: res[2], src: res[0] }));
      }


    }

  }
};


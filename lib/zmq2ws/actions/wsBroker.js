import WebSocket from 'ws';
import { ON_WS_BROKER_SUB, ON_WS_BROKER_UNSUB } from "../actionTypes/wsBroker";

export const onWebSocketSub = (ws, zmq$, name) => (dispatch, getState) => {
    const state = getState();
    let zmqUnSub = zmq$.subscribe(payload => {
        if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify({ type: "zmq", src: name.substring(0, name.indexOf("|")), payload }));
        } else {
            zmqUnSub.unsubscribe();
            zmqUnSub = null;
        }
    });
    dispatch({
        type: ON_WS_BROKER_SUB,
        name,
        zmqUnSub,
        ws
    });

}

export const onWebSocketUnSub = (ws, name) => (dispatch, getState) => {
    const state = getState();
    dispatch({
        type: ON_WS_BROKER_UNSUB,
        name,
        ws
    });
}

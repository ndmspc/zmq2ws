import { ON_APP_START, ON_WS_ENTER, ON_WS_EXIT, ON_WS_USER_LOGIN, ON_WS_USER_LOGOUT, ON_SERVICE_ADD, ON_SERVICE_REMOVE_BY_GROUP, ON_SERVICE_REMOVE_SINGLE } from "../actionTypes/appInfo";
import { getAppInfoForWebSocket } from '../selectors/appInfo';

// actions

export const onAppStart = data => (dispatch) => {
    dispatch({
        type: ON_APP_START,
        payload: data
    })
};

export const onWebSocketEnter = (wss, userInfo) => (dispatch, getState) => {
    dispatch({
        type: ON_WS_ENTER,
        userInfo
    })
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onWebSocketExit = (wss, userInfo) => (dispatch, getState) => {
    dispatch({
        type: ON_WS_EXIT,
        userInfo
    })
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onWebSocketUserLogin = (wss, id, username, uid) => (dispatch, getState) => {
    dispatch({
        type: ON_WS_USER_LOGIN,
        id,
        username,
        uid
    })
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onWebSocketUserLogout = (wss, id) => (dispatch, getState) => {
    dispatch({
        type: ON_WS_USER_LOGOUT,
        id
    })
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onZeroMQServiceAdd = (data, wss) => (dispatch, getState) => {
    dispatch({
        type: ON_SERVICE_ADD,
        payload: data,
        wss
    });
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onZeroMQServiceRemove = (data, wss) => (dispatch, getState) => {
    dispatch({
        type: ON_SERVICE_REMOVE_BY_GROUP,
        payload: data,
        wss
    });
    wss.broadcast(getAppInfoForWebSocket(getState()));
};

export const onZeroMQServiceRemoveSingle = (data, wss) => (dispatch, getState) => {
    dispatch({
        type: ON_SERVICE_REMOVE_SINGLE,
        payload: data,
        wss
    });
    wss.broadcast(getAppInfoForWebSocket(getState()));
      
};

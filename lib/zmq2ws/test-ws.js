import WebSocket from "ws";

function help() {
  console.log(
    process.argv[0] +
      " " +
      process.argv[1] +
      " <zmq-url> <name> <sub> <src>"
  );
}
if (process.argv.length < 2) {
  help();
  process.exit(1);
} else {
  var url = process.argv[2] || "ws://localhost:8442";
  var name = process.argv[3] || "zmq2ws";
  var sub = process.argv[4] || "test";
  var src = process.argv[5] || "zmq2wstest";

  console.log("Connecting to '", url, "' ...");
  const ws = new WebSocket(url);
  ws.on("open", function open() {
    let object = {
      type: "sub",
      name,
      sub,
      src
    };
    ws.send(JSON.stringify(object));

    setTimeout(function() {
      let object = {
        type: "unsub",
        name,
        sub,
        src
      };
      console.log("Unsub from '" + object.name + "' '" + object.sub + "' ...");
      ws.send(JSON.stringify(object));
      setTimeout(function() {
        console.log("Disconnected ...");
        ws.close();
        process.exit(0);
      }, 5000);
    }, 10000);
  });

  ws.on("message", function incoming(data) {
    console.log(data);
  });
}

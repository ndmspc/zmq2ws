import zmq from "zeromq";
const zmq_sock = zmq.socket("pub");
function help() {
    console.log(
        process.argv[0] +
        " " +
        process.argv[1] +
        " <zmq-url> <cmd>"
    );
}

var i = 0;
if (process.argv.length < 3) {
    help();
    process.exit(1);
} else {

    let url = process.argv[2] || "tcp://localhost:41000";;
    let cmd = process.argv[3] || "WORKER_COUNT";
    let zmq_dealer = zmq.socket('dealer');
    console.log("ZMQ connected to " + url);
    zmq_dealer.connect(url);
    console.log(cmd);
    // process.exit(0);
    //   zmq_dealer.send(["", "AUTH"]);
    zmq_dealer.send(["AUTH"]);
    zmq_dealer.on('message', (empty, c, r) => {
        console.log("msg is here");
        let c_str = c.toString();
        let r_str = r.toString();
        console.log(c_str);
        console.log(r_str);
        if (c_str === "AUTH" && r_str === "OK") {
            console.log("AUTH is OK");
            if (cmd !== "AUTH") {
                zmq_dealer.send([cmd]);
            }
            console.log(cmd + " sent")
        } else if (c_str === cmd && r_str === "OK") {
            console.log(cmd + " is OK")
            zmq_dealer.disconnect(url);
            process.exit(0);
        } else {
            zmq_dealer.disconnect(url);
            process.exit(0);
        }
    });

}

process.on("SIGINT", () => {
    console.log();
    console.log("Exiting ...");
    process.exit(1);
});






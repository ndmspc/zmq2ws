import { exec } from "child_process";
import { name, version } from "../../package.json";
console.log(name + " " + version);

import config from "config-yml";

import app from "./server";
import zmq from "zeromq";

import store from "./store/configureStore";
import * as appInfo from "./actions/appInfo";
import * as zmqBroker from "./actions/zmqBroker";

var url_init =
  "tcp://" + config.discovery.input.host + ":" + config.discovery.input.port;
var url_publisher =
  "tcp://" +
  config.discovery.publish.host +
  ":" +
  config.discovery.publish.port;

const zmq_discovery_init = zmq.socket("req");
const zmq_discovery = zmq.socket("sub");

if (url_init !== undefined && url_init !== 0) {
  console.log("ZMQ connected to " + url_init);
  zmq_discovery_init.connect(url_init);
  zmq_discovery_init.send(["get", "", "", ""]);
  zmq_discovery_init.on("message", (data) => {
    let dataObj = JSON.parse(data.toString());
    dataObj.data.forEach((item) => {
      console.log(item);
      store.dispatch(zmqBroker.onZeroMQAdd(item, app.wss));
    });
    zmq_discovery_init.disconnect(url_init);
  });
}

if (url_publisher !== undefined && url_publisher.length !== 0) {
  console.log(
    "ZMQ connected to " +
      url_publisher +
      " sub='" +
      config.discovery.publish.sub +
      "'"
  );
  zmq_discovery.connect(url_publisher);
  zmq_discovery.subscribe(config.discovery.publish.sub);
  zmq_discovery.on("message", (data) => {
    let dataObj = JSON.parse(data.toString());
    console.log(dataObj);
    if (dataObj.action === "add") {
      store.dispatch(zmqBroker.onZeroMQAdd(dataObj.zmq, app.wss));
    } else if (dataObj.action === "rm") {
      store.dispatch(zmqBroker.onZeroMQSubRemoveByGroup(dataObj.zmq));
      store.dispatch(zmqBroker.onZeroMQRemove(dataObj.zmq, app.wss));
    } else {
      console.log("Error : [zmq_discovery] unknown type=" + dataObj.type);
    }
  });
}

var myVar = setInterval(
  myTimer,
  process.env.ZMQ2WS_INACTIVITY_CHECK_MSEC || 60 * 1000 /** 1 min */
);
function myTimer() {
  store.dispatch(
    zmqBroker.onZeroMQSubCleanup(
      process.env.ZMQ2WS_INACTIVITY_TIMEOUT_SEC || 3600 /** 1 hour */,
      app.wss
    )
  );
}

store.dispatch(appInfo.onAppStart({ name, version }));
app.server.listen(app.get("port"), () =>
  console.log(
    "Http" +
      (config.zmq2ws.ssl.enabled ? "s" : "") +
      " is listening on port " +
      app.get("port")
  )
);

process.on("SIGINT", () => {
  console.log();
  console.log("Exiting ...");
  zmq_discovery.unsubscribe(config.discovery.publish.sub);
  zmq_discovery.unbind(url_publisher);
  process.exit(1);
});

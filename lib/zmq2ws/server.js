import fs from 'fs';
// import uuid from 'uuid';
import { v4 as uuidv4 } from 'uuid';
import http from 'http';
import https from 'https';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import geoip from 'geoip-lite';
import useragent from 'useragent';
import WebSocket from 'ws';

import config from 'config-yml';

import store from './store/configureStore';
import * as appInfo from './actions/appInfo';
import * as wsBroker from './actions/wsBroker';
import { getZeroMQBroker } from './selectors/zmqBroker';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({ allowedOrigins: '*' }));
app.disable('x-powered-by');

if (config.zmq2ws.ssl.enabled) {
    let privateKey = fs.readFileSync(config.zmq2ws.ssl.key, 'utf8');
    let certificate = fs.readFileSync(config.zmq2ws.ssl.cert, 'utf8');
    let credentials = { key: privateKey, cert: certificate };
    app.server = https.createServer(credentials, app);
    app.set('port', config.zmq2ws.port);

} else {
    app.server = http.createServer(app);
    app.set('port', config.zmq2ws.port);
}

app.wss = new WebSocket.Server({ server: app.server });

app.wss.broadcast = function broadcast(data) {
    app.wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};

app.wss.on('connection', (ws, req) => {

    // generate id of websocket
    ws.id = uuidv4();

    // get number of clients
    let nclients = Array.from(app.wss.clients).length;
    console.log('[' + nclients + '] User ' + ws.id + ' was connected');

    const ip = req.headers['x-real-ip'] || req.connection.remoteAddress || null;
    const geo = ip ? geoip.lookup(ip) : null;
    // get ip and user info
    let userInfo = {
        id: ws.id,
        ip,
        geo,
        info: useragent.lookup(req.headers['user-agent']).toJSON(),
        username: ""
    };
    store.dispatch(appInfo.onWebSocketEnter(app.wss, userInfo));

    let wsInfo = {
        type: "ws",
        payload: { id: ws.id }
    };
    ws.send(JSON.stringify(wsInfo));

    ws.on('message', (msg) => {
        console.log('received: %s', msg);
        let o = JSON.parse(msg);
        let n = "";
        if (o.src === undefined) o.src = "obmon";
        if (o.type === "sub") {
            let b = getZeroMQBroker(store.getState()).zmqs$;
            n = o.src + "|" + o.name + "|" + o.sub;
            if (b[n] !== undefined) {
                if (b[n].zmq$ !== undefined) {
                    store.dispatch(wsBroker.onWebSocketSub(ws, b[n].zmq$, n));
                }
            } else {
                console.log("Ws:onMesage => We could not find service : " + n);
            }
        } else if (o.type === "unsub") {
            n = o.src + "|" + o.name + "|" + o.sub;
            store.dispatch(wsBroker.onWebSocketUnSub(ws, n));
        } else if (o.type === "updateinfo") {
            console.log("Info update for ws "+ ws.id + " username: "+ o.username + " uid: "+ o.uid );
            store.dispatch(appInfo.onWebSocketUserLogin(app.wss, ws.id, o.username, o.username));
        } else {
            // ws.send(JSON.stringify({ type: "zmq", src: "" }));
        }
    });

    ws.on('close', () => {
        store.dispatch(appInfo.onWebSocketExit(app.wss, { id: ws.id }));
        let nclients = Array.from(app.wss.clients).length;
        console.log('[' + nclients + '] User ' + ws.id + ' was disconnected');
        store.dispatch(wsBroker.onWebSocketUnSub(ws, ""));
    });

    ws.on('error', () => console.log('Error from web socket'));
});


app.get('*', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

export default app;

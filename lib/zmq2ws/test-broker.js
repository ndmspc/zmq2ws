import zmq from "zeromq";
const zmq_sock = zmq.socket("pub");
function help() {
  console.log(
    process.argv[0] +
    " " +
    process.argv[1] +
    " <zmq-url> <name> <sub> <src> <timeout>"
  );
}
var i = 0;
if (process.argv.length < 2) {
  help();
  process.exit(1);
} else {
  var zmq_url = process.argv[2];

  zmq_sock.bindSync(zmq_url);
  var name = process.argv[3] || "";
  var sub = process.argv[4] || "";
  var src = process.argv[5] || "zmq2wstest";
  var timeout = process.argv[6] || 1000;

  name = src + ":" + name;
  console.log('Sensor is connecting to "' + zmq_url + '" timeout=' + timeout);
  let timerid = setInterval(() => {
    console.log("Sending : " + name + " " + sub + " data=" + JSON.stringify({ "count": i }));
    zmq_sock.send([name, sub, JSON.stringify({ "count": i })]);
    i++;
  }, timeout);
}

process.on("SIGINT", () => {
  console.log();
  console.log("Exiting ...");
  process.exit(1);
});

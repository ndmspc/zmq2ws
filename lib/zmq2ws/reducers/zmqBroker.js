import { ON_ZMQ_BROKER_ADD, ON_ZMQ_BROKER_REMOVE, ON_ZMQ_BROKER_ZMQSUB_ADD, ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_GROUP, ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_FULLNAME } from "../actionTypes/zmqBroker";

const initialState = {
    zmqs: {},
    zmqs$: {}
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case ON_ZMQ_BROKER_ADD:
            // console.log(action.zmq);
            return { ...state, zmqs: { ...state.zmqs, [action.payload.name]: { zmq: action.zmq, url: action.payload.url } } };
        case ON_ZMQ_BROKER_REMOVE:
            return { ...state, zmqs: { ...state.zmqs, [action.payload.name]: undefined } };
        case ON_ZMQ_BROKER_ZMQSUB_ADD:
            return { ...state, zmqs$: { ...state.zmqs$, [action.payload.src + "|" + action.payload.name + "|" + action.payload.sub]: { zmq$: action.zmq$, name: action.payload.name } } };
        case ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_GROUP:
            let { ...clone } = state.zmqs$;
            for (var p in clone) {
                if (clone.hasOwnProperty(p)) {
                    if (p.startsWith(action.payload.src + "|" + action.payload.name)) {
                        delete clone[p];
                    }
                }
            }
            return { ...state, zmqs$: clone };
        case ON_ZMQ_BROKER_ZMQSUB_REMOVE_BY_FULLNAME:
            let { ...clone_byname } = state.zmqs$;
            for (var p in clone_byname) {
                if (clone_byname.hasOwnProperty(p)) {
                    if (p === action.payload.name) {
                        delete clone_byname[p];
                    }
                }
            }
            return { ...state, zmqs$: clone_byname };
        default:
            return state;
    }
}

import WebSocket from 'ws';
import { ON_WS_BROKER_SUB, ON_WS_BROKER_UNSUB  } from "../actionTypes/wsBroker";

// init state
const initialState = {
    wsClients: {}
};

// reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case ON_WS_BROKER_SUB: {
            // TODO check if subscribed already
            let clients = { ...state.wsClients };
            if (clients[action.ws.id] !== undefined) {
                if (clients[action.ws.id][action.name] !== undefined) {
                    clients[action.ws.id][action.name].unsubscribe();
                    delete clients[action.ws.id][action.name];
                }
            } else {
                clients[action.ws.id] = {};
            }
            clients[action.ws.id][action.name] = action.zmqUnSub;
            return { ...state, wsClients: clients };
        }
        case ON_WS_BROKER_UNSUB: {
            if (state.wsClients[action.ws.id] === undefined) {
                return state;
            }
            let clients = { ...state.wsClients };
            if (action.name === "") {
                for (var o in clients[action.ws.id]) {
                    if (clients[action.ws.id].hasOwnProperty(o)) {
                        // TODO move it to action
                        clients[action.ws.id][o].unsubscribe();
                        delete clients[action.ws.id][o];
                    }
                }
            } else {
                if (clients[action.ws.id][action.name] !== undefined) {
                    // TODO move it to action
                    clients[action.ws.id][action.name].unsubscribe();
                    delete clients[action.ws.id][action.name];
                }
            }
            if (Object.keys(clients[action.ws.id]).length === 0 && clients[action.ws.id].constructor === Object) {
                delete clients[action.ws.id];
            }
            return { ...state, wsClients: clients };
        }
        default:
            return state;
    }
}

import {
    ON_APP_START,
    ON_WS_ENTER,
    ON_WS_EXIT,
    ON_WS_USER_LOGIN,
    ON_WS_USER_LOGOUT,
    ON_SERVICE_ADD,
    ON_SERVICE_REMOVE_BY_GROUP,
    ON_SERVICE_REMOVE_SINGLE,
} from "../actionTypes/appInfo";

// init state
const initialState = {
    name: "",
    version: "",
    clients: [],
    services: [],
};

// reducer
export default function reducer(state = initialState, action) {
    let clients = [];
    let clientArray = [];
    let userIndex = 0;
    let services = [];

    switch (action.type) {
        case ON_APP_START:
            let { name, version } = action.payload;
            console.log("Starting '" + name + " " + version + "' ...");
            return { ...state, name, version };
        case ON_WS_ENTER:
            clients = [...state.clients];
            clientArray = clients.map((item) => item.id);
            userIndex = clientArray.indexOf(action.userInfo.id);
            if (userIndex === -1) {
                return { ...state, clients: [...clients, action.userInfo] };
            }
            console.log("Error: in redusers/appInfo. This should not happen");
            return state;
        case ON_WS_USER_LOGIN:
            clients = [...state.clients];
            clientArray = clients.map((item) => item.id);
            userIndex = clientArray.indexOf(action.id);
            if (userIndex !== -1) {
                clients[userIndex]["username"] = action.username;
                clients[userIndex]["uid"] = action.uid;
                return { ...state, clients };
            }
            return state;
        case ON_WS_USER_LOGOUT:
            clients = [...state.clients];
            clientArray = clients.map((item) => item.id);
            userIndex = clientArray.indexOf(action.id);
            if (userIndex !== -1) {
                clients[userIndex].userInfo.username = "";
                return { ...state, clients };
            }
            return state;
        case ON_WS_EXIT:
            clients = [...state.clients];
            clients = clients.filter((item) => item.id !== action.userInfo.id);
            return { ...state, clients };
        case ON_SERVICE_ADD:
            services = [...state.services];
            services = services.filter(
                (item) =>
                    item.name === action.payload.name &&
                    item.sub === action.payload.sub &&
                    item.src === action.payload.src
            );
            if (services.length === 0)
                return {
                    ...state,
                    services: [
                        ...state.services,
                        {
                            name: action.payload.name,
                            sub: action.payload.sub,
                            src: action.payload.src,
                        },
                    ],
                };
            return state;
        case ON_SERVICE_REMOVE_BY_GROUP:
            services = [...state.services];
            services = services.filter((item) => item.name !== action.payload.name);
            return {
                ...state,
                services,
            };
        case ON_SERVICE_REMOVE_SINGLE:
            services = [...state.services];
            services = services.filter((item) => !(item.name === action.payload.name &&
                item.sub === action.payload.sub &&
                item.src === action.payload.src));
            return {
                ...state,
                services,
            };
        default:
            return state;
    }
}

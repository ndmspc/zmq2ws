export const getAppInfo = (state) => state.appInfo;
export const getAppInfoForWebSocket = (state) => {
    return JSON.stringify({
        type: "app",
        payload: getAppInfo(state)
    });
}


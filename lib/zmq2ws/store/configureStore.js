import { createStore, applyMiddleware, combineReducers,compose } from 'redux';
import thunk from 'redux-thunk';
import createCLILogger from 'redux-cli-logger'

import appInfo from '../reducers/appInfo';
import zmqBroker from '../reducers/zmqBroker';
import wsBroker from '../reducers/wsBroker';

const reducer = combineReducers({
    appInfo,
    zmqBroker,
    wsBroker
});

const middleware = [
    thunk
    // your middleware here
]

if (process.env.NODE_ENV === 'development') {
    console.log("Development mode is enabled");
    const loggerOptions = {
    }
    const logger = createCLILogger(loggerOptions)
    middleware.push(logger)
}

const enhancer = compose(
    applyMiddleware(...middleware)
    // optionally, electron-enhancer, redux-loop, etc.
)

const configureStore = (initialState) => createStore(reducer, initialState,enhancer);

const store = configureStore();
export default store;

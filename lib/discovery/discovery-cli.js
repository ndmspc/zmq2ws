import zmq from "zeromq";
const zmq_sock = zmq.socket("req");
function help() {
  console.log(
    process.argv[0] +
      " " +
      process.argv[1] +
      " <discovery-url> <action> <name> <url> <src>"
  );
  console.log("     action : add rm get");
}
if (process.argv.length < 3) {
  help();
  process.exit(1);
} else {
  var zmq_url = process.argv[2];
  console.log('Discovery cli is connecting to "' + zmq_url + '"');
  zmq_sock.connect(zmq_url);
  var action = process.argv[3] || "";
  var name = process.argv[4] || "";
  var url = process.argv[5] || "";
  var src = process.argv[6] || "obmon";
  var cache = process.argv[7] || 1;
  if (action === "add" || action === "rm" || action === "get") {
    console.log("Sending : " + action + " " + name + " " + url + " " + src);
    zmq_sock.send([action, name, url, src, cache]);
  } else {
    console.log("action '" + action + "' is not supported\n");
    help();
    process.exit(1);
  }

  zmq_sock.on("message", (data) => {
    const res = JSON.parse(data.toString());
    if (res.data !== undefined) {
      let arr = res.data;
      arr.forEach(function (item) {
        console.log(item);
      });
    }
    console.log(res.msg);
    zmq_sock.disconnect(zmq_url);
    process.exit(res.rc);
  });
}
process.on("SIGINT", () => {
  console.log();
  console.log("Exiting ...");
  zmq_sock.disconnect(zmq_url);
  process.exit(1);
});

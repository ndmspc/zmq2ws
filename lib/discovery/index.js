import zmq from "zeromq";

import store from "./store/configureStore";
import * as zmqServices from "./actions/zmqServices";
import { getZeroMQServices } from "./selectors/zmqServices";

import config from "config-yml";

var init_services = config.discovery.services;

const zmq_in = zmq.socket("rep");
const zmq_out = zmq.socket("pub");

var url_in = "tcp://*:" + config.discovery.input.port;
var url_out = "tcp://*:" + config.discovery.publish.port;

console.log('Discovery is waiting for connection at "' + url_in);
zmq_in.bindSync(url_in);
zmq_out.bindSync(url_out);

if (init_services !== undefined) {
  console.log("Default services: ");
  init_services.forEach((element) => {
    console.log(element);
    store.dispatch(zmqServices.onZeroMQServiceAdd(element));
    zmq_out.send(JSON.stringify({ action: "add", zmq: element }));
  });
}

zmq_in.on("message", (a, n, u, s, c) => {
  const action = a.toString();
  const name = n.toString();
  const url = u.toString();
  const src = s.toString();
  const cache = c;
  if (action === "add") {
    if (name.length !== 0) {
      const out = {
        name,
        url,
        src,
        cache,
      };
      store.dispatch(zmqServices.onZeroMQServiceAdd(out));
      // TODO to put it to store
      zmq_out.send(JSON.stringify({ action, zmq: out }));
    }
    zmq_in.send(JSON.stringify({ rc: 0, msg: "OK" }));
  } else if (action === "rm") {
    if (name.length !== 0) {
      const out = {
        name,
        url,
        src,
        cache,
      };
      store.dispatch(zmqServices.onZeroMQServiceRemove(out));
      // TODO to put it to store
      zmq_out.send(JSON.stringify({ action, zmq: out }));
    }
    zmq_in.send(JSON.stringify({ rc: 0, msg: "OK" }));
  } else if (action === "get") {
    // TODO to put it to store
    zmq_in.send(
      JSON.stringify({
        rc: 0,
        msg: "OK",
        data: getZeroMQServices(store.getState()).services,
      })
    );
  }
});

process.on("SIGINT", () => {
  console.log();
  console.log("Exiting ...");
  zmq_in.unbind(url_in);
  zmq_out.unbind(url_out);
  process.exit(1);
});

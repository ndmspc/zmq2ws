import { ON_ZMQ_ADD, ON_ZMQ_REMOVE } from "../actionTypes/zmqServices";

// initial state
const initialState = {
    services: []
};

// reducer
export default function reducer(state = initialState, action) {

    switch (action.type) {
        case ON_ZMQ_ADD:
            if (state.services.filter(item => item.name === action.name).length === 0)
                return { ...state, services: [...state.services, { name: action.name, url: action.url, src: action.src, cache: action.cache }] }
            else
                return state;
        case ON_ZMQ_REMOVE:
            return { ...state, services: state.services.filter(item => item.name !== action.name) }
        default:
            return state;
    }
}

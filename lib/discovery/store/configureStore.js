import { createStore, applyMiddleware, combineReducers,compose } from 'redux';
import createCLILogger from 'redux-cli-logger'

import zmqServices from '../reducers/zmqServices';

const reducer = combineReducers({
    zmqServices
});

const middleware = [
    // your middleware here
]

if (process.env.NODE_ENV === 'development') {
    console.log("Development mode is enabled");
    const loggerOptions = {
    }
    const logger = createCLILogger(loggerOptions)
    middleware.push(logger)
}

const enhancer = compose(
    applyMiddleware(...middleware)
    // optionally, electron-enhancer, redux-loop, etc.
)

const configureStore = (initialState) => createStore(reducer, initialState,enhancer);

const store = configureStore();
export default store;

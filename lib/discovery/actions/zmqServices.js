import { ON_ZMQ_ADD, ON_ZMQ_REMOVE } from "../actionTypes/zmqServices";

// actions
export const onZeroMQServiceAdd = data => ({
    type: ON_ZMQ_ADD,
    name: data.name,
    url: data.url,
    src: data.src,
    cache: data.cache || 1
});

export const onZeroMQServiceRemove = data => ({
    type: ON_ZMQ_REMOVE,
    name: data.name
});

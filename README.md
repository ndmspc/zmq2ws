# zmq2ws

## Running via podman/docker
Run service with default values
```
podman run --net=host registry.gitlab.com/ndmspc/zmq2ws
```
With custom config file (config.yaml)
```
podmab run --net=host -v config.yml:/app/config.yml registry.gitlab.com/ndmspc/zmq2ws
```

## Development

### Install

```
npm install
```

### Starting

```
npm start
```
More verbose
```
NODE_ENV=development npm start
```

## Testing
Start server
```
NODE_ENV=development npm start
```
Start testing broker
```
NODE_ENV=development npm run test-broker
```
Test wwebsocket
```
NODE_ENV=development npm run test-ws
```

## Start zmq2ws and discovery separatly
Start discovery
```
NODE_ENV=development npm run discovery
```
Start zmq2ws
```
NODE_ENV=development npm run zmq2ws
```
